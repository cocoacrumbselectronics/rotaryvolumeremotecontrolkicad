

difference() {
    cylinder(h = 16, r1 = 10, r2 = 10, center = false, $fn=360);

    // shaft
    difference() {
        cylinder(h = 16, r1 = 3.15, r2 = 3.15, center = false, $fn=360);
        translate([-3, 1.45, -0.5]) cube(size = [6,5,17], center = false);
    }
}
